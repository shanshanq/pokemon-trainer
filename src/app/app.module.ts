import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { AppRoutingModule } from './app-routing.module';
import { UserLoginPage } from './user-login/user-login.page';
import { PokemonCataloguePage } from './pokemon-catalogue/pokemon-catalogue.page';
import { PokemonTrainerPage } from './pokemon-trainer/pokemon-trainer.page';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AuthGuardService as AuthGuard } from './services/auth-guard.service';


@NgModule({
  declarations: [
    AppComponent,
    UserLoginPage,
    PokemonCataloguePage,
    PokemonTrainerPage,
    NavbarComponent,
    FooterComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
