import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { NotFoundComponent } from "./not-found/not-found.component";
import { PokemonCataloguePage } from "./pokemon-catalogue/pokemon-catalogue.page";
import { PokemonTrainerPage } from "./pokemon-trainer/pokemon-trainer.page";
import { UserLoginPage } from "./user-login/user-login.page";
import { AuthGuardService as AuthGuard } from "./services/auth-guard.service";


const routes:Routes = [
    {
        path:'',
        pathMatch:'full',
        redirectTo:'/login'
    },
    {
        path:'login',
        component:UserLoginPage
    },
    {
        path:'catalogue',
        component:PokemonCataloguePage,
        canActivate: [AuthGuard] 
    },
    {
        path:'collection',
        component:PokemonTrainerPage,
        canActivate: [AuthGuard] 
    },
    {
        path:'**',
        component: NotFoundComponent
    }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]

})

export class AppRoutingModule{}