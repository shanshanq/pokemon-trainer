export interface Pokemon{
    name: string;
    url: string;
    id:string;
    image: string;
    isSelected: boolean
}