import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { Pokemon } from "../models/pokemon.model";


@Component({
    selector:"app-pokemon-trainer",
    templateUrl:"pokemon-trainer.page.html",
    styleUrls:["pokemon-trainer.page.css"]
})

export class PokemonTrainerPage implements OnInit{
    private _collection:Pokemon[]|null  = []
    constructor(private router: Router){
    }
    ngOnInit():void{
        // Get the collection from the local storage
        let collection: string | null = localStorage.getItem("collection")
        if (collection === null) {
            this._collection = null
        } else{
            this._collection = JSON.parse(collection)
        }   
    }
    get collection(){
        return this._collection
    }

    logOut():void{
        // Clear the local storage, and back to the login page
        localStorage.removeItem("user");
        localStorage.removeItem("collection");
        this.router.navigate(["login"])
    }
   
}