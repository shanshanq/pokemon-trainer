import { Component, NgModule } from "@angular/core";
import { NgForm, NgModel } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
    selector:'app-user-login',
    templateUrl:'./user-login.page.html',
    styleUrls:['./user-login.page.css']
})
export class UserLoginPage{

    constructor(private router : Router){

    }

    public onSubmit(username:NgModel): void{
        // Store the username in local storage, and then redirect to the main page, the Pokémon Catalogue page
        localStorage.setItem("user",JSON.stringify({username:username.value }))
        this.router.navigate(["catalogue"])
        
    }

}