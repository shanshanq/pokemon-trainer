import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Pokemon } from "../models/pokemon.model";
import { map,catchError} from 'rxjs/operators';
import { throwError } from "rxjs";

@Injectable({
    providedIn:'root'
})
export class PokemonService{

    private _pokemons : Pokemon[] = []
    private _error : string = ''

    
    constructor(private readonly http:HttpClient){
    }

    public fetchPokemons(): void{
        this.http.get<Pokemon[]>('https://pokeapi.co/api/v2/pokemon?limit=151')
        .pipe(
            map((response:any) =>{
                // Modify the response data, add high quality images of pokemons
                return response['results'].map((res:any)=>{
                    let urls = res.url.split('/');
                    let id = urls[urls.length-2]
                    return {
                        ...res,
                        id:id,
                        image : "https://raw.githubusercontent.com/PokeAPI/sprites/84204f8594790cfd04190a8d82beb31f49115c02/sprites/pokemon/other/dream-world/"+id+".svg",
                        isSelected: false
                    }
                })
            }),
            catchError( error => {
                return throwError(error);
            })
        )
        .subscribe(
            (modified:any)=>{
                this._pokemons = modified;
         },(error:HttpErrorResponse)=>{
                this._error = error.message
            }                    
        )
        
    }

    public updatePokemons(id:string): void{
        // Update the pokemon with given id
        this._pokemons.forEach( item =>{
            if(item.id === id){
                item.isSelected = true
            }
        })
    }
    
    public pokemons() :Pokemon[]{
        return this._pokemons;
    }

    public error(): string{
        return this._error;
    }

}