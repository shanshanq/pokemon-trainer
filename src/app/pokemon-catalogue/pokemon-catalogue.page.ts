import { Component, OnInit  } from "@angular/core";
import { PokemonService } from "../services/pokemon.service";
import { Pokemon } from "../models/pokemon.model";


@Component({
    selector:'app-pokemon-catalogue',
    templateUrl:'./pokemon-catalogue.page.html',
    styleUrls:['./pokemon-catalogue.page.css']

})

export class PokemonCataloguePage implements OnInit{
    private _pokemons: Pokemon[] =[]
    private _selected: Pokemon[] | null = []

    constructor(private readonly pokemonService: PokemonService){}

    ngOnInit(){
        this.pokemonService.fetchPokemons();
        // Get the collection from the local storage
        let collection: string | null= localStorage.getItem("collection")
        if (collection === null) {
            this._selected = null
        } else{
            this._selected = JSON.parse(collection)
        }     
    }
    
    ngDoCheck(){
        // Called immediately after ngOnInit() on the first run
        // Update the collected pokemons state
        if(this._selected !== null){
            this._selected.map(x=>{
                this.pokemonService.updatePokemons(x.id)
            })
        }
    }
    get pokemons(): Pokemon[]{
        return this.pokemonService.pokemons();
    }

    public onClick(pokemon: Pokemon){
        this.pokemonService.updatePokemons(pokemon.id)

        if(this._selected === null){
            this._selected=[pokemon]
        }else{
            this._selected=[...this._selected,pokemon]
        }
        // Add the current collection to local storage
        localStorage.setItem("collection", JSON.stringify(this._selected))
    }
}